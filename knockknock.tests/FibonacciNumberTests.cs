﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace knockknock.tests
{
    [TestClass]
    public class FibonacciNumberTests
    {
        [TestMethod]
        public void TestBasicUsage()
        {
            var service = new RedPill();

            // A Fibonacci sequence is the sequence of numbers 1, 1, 2, 3, 5, 8, 13, 21, 34, etc.
            long number = service.FibonacciNumber(5);

            Assert.AreEqual(8, number, "fibonacci number not correct.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestNegativeInput()
        {
            var service = new RedPill();
            service.FibonacciNumber(-5);
        }
    }
}
