﻿using System;
using knockknock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace knockknock.tests
{
    [TestClass]
    public class WhoAreYouTests
    {
        [TestMethod]
        public void TestWhoAmI()
        {
            var service = new RedPill();
            var contact = service.WhoAreYou();

            Assert.AreEqual(contact, ContactDetails.Me(), "WhoAreYou returns the incorrect value");
        }
    }
}
