﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace knockknock.tests
{
    [TestClass]
    public class TriangeServiceTests
    {
        [TestMethod]
        public void TestEquilateralTriangle()
        {
            var service = new TriangleTypeService();
            var triangle = service.CalculateTriangle(3, 3, 3);

            Assert.AreEqual(TriangleType.Equilateral, triangle);
        }

        [TestMethod]
        public void TestIsoscelesTriangle()
        {
            var service = new TriangleTypeService();
            var triangle = service.CalculateTriangle(3, 3, 10);

            Assert.AreEqual(TriangleType.Isosceles, triangle);
        }

        [TestMethod]
        public void TestScaleneTriangle()
        {
            var service = new TriangleTypeService();
            var triangle = service.CalculateTriangle(3, 6, 9);

            Assert.AreEqual(TriangleType.Scalene, triangle);
        }
    }
}
