﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using knockknock;

namespace knockknock.tests
{
    [TestClass]
    public class StringServiceTests
    {
        [TestMethod]
        public void TestReverse()
        {
            var service = new StringService();
            string result = service.ReverseWords("this is a test");

            Assert.AreEqual("test a is this", result);
        }

        [TestMethod]
        public void TestStringWithPrependedSpaces()
        {
            var service = new StringService();
            string result = service.ReverseWords("    this is a test");

            Assert.AreEqual("test a is this    ", result);
        }

        [TestMethod]
        public void TestStringWithPostpendedSpaces()
        {
            var service = new StringService();
            string result = service.ReverseWords("this is a test    ");

            Assert.AreEqual("    test a is this", result);
        }
    }
}
