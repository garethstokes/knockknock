﻿using System;
using System.Threading.Tasks;
namespace knockknock
{
    public class RedPill : IRedPill
    {
        public ContactDetails MyContactDetails { get; set; }

        public RedPill()
        {
            MyContactDetails = ContactDetails.Me();
        }

        public ContactDetails WhoAreYou()
        {
            return MyContactDetails;
        }

        public Task<ContactDetails> WhoAreYouAsync()
        {
            return Task.Factory.StartNew<ContactDetails>(() => MyContactDetails);
        }

        // A Fibonacci sequence is the sequence of numbers 1, 1, 2, 3, 5, 8, 13, 21, 34, etc.
        // where each number (from the third on) is the sum of the previous two.
        public long FibonacciNumber(long n)
        {
            if (n > long.MaxValue || n < 0)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (n <= 1) return 1;
            return FibonacciNumber(n - 1) + FibonacciNumber(n - 2);
        }

        public Task<long> FibonacciNumberAsync(long n)
        {
            return Task.Factory.StartNew<long>(() => FibonacciNumber(n));
        }

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            var service = new TriangleTypeService();
            return service.CalculateTriangle(a, b, c);
        }

        public Task<TriangleType> WhatShapeIsThisAsync(int a, int b, int c)
        {
            return Task.Factory.StartNew<TriangleType>(() => WhatShapeIsThis(a, b, c));
        }

        public string ReverseWords(string s)
        {
            var service = new StringService();
            return service.ReverseWords(s);
        }

        public Task<string> ReverseWordsAsync(string s)
        {
            return Task.Factory.StartNew<string>(() => ReverseWords(s));
        }
    }
}
