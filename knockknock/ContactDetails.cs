﻿using System.Diagnostics;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;

namespace knockknock
{
    [DebuggerStepThroughAttribute()]
    [GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [DataContractAttribute(Name = "ContactDetails", Namespace = "http://KnockKnock.readify.net")]
    public partial class ContactDetails : object // do we need this?, IExtensibleDataObject
    {
        [DataMemberAttribute()]
        public string EmailAddress { get; set; }

        [DataMemberAttribute()]
        public string FamilyName { get; set; }

        [DataMemberAttribute()]
        public string GivenName { get; set; }

        [DataMemberAttribute()]
        public string PhoneNumber { get; set; }

        public override bool Equals(object obj)
        {
            ContactDetails contact = (ContactDetails)obj;
            if (contact == null) return false;

            if (contact.EmailAddress != EmailAddress) return false;
            if (contact.GivenName != GivenName) return false;
            if (contact.FamilyName != FamilyName) return false;
            if (contact.PhoneNumber != PhoneNumber) return false;

            return true;
        }

        private static ContactDetails _me;
        public static ContactDetails Me()
        {
            if (_me == null) {
                _me = new ContactDetails {
                    EmailAddress    = "gareth@betechnology.com.au",
                    GivenName       = "Gareth",
                    FamilyName      = "Stokes",
                    PhoneNumber     = "0414 072 676"
                };
            }

            return _me;
        }
    }
}