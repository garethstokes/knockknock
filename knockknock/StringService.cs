﻿using System.Collections.Generic;
using System.Text;

namespace knockknock
{
    public class StringService
    {
        public string ReverseWords(string s)
        {
            var stack = new Stack<string>(s.Split(new [] { ' ' }));
            var builder = new StringBuilder();

            while (stack.Count > 0)
            {
                builder.Append(stack.Pop());
                if (stack.Count > 0) builder.Append(' ');
            }

            return builder.ToString();
        }
    }
}