﻿

using System.Collections.Generic;
namespace knockknock
{
    public class TriangleTypeService
    {
        /// <summary>
        /// Pass in the interger numbers and this function will return the type
        /// of triangle that would be created. 
        /// </summary>
        /// <returns>
        /// One of these 4 values. 
        ///   TriangleType.Isosceles
        ///   TriangleType.Equilateral
        ///   TriangleType.Scalene
        ///   TriangleType.Error
        /// </returns>
        public TriangleType CalculateTriangle(int a, int b, int c)
        {
            // create a dictionary to store a key; 
            var dict = new Dictionary<int, int>();
            dict[a] = 1;
            dict[b] = 1;
            dict[c] = 1;

            if (dict.Keys.Count == 3) return TriangleType.Scalene;
            if (dict.Keys.Count == 2) return TriangleType.Isosceles;
            if (dict.Keys.Count == 1) return TriangleType.Equilateral;

            return TriangleType.Error;
        }
    }
}